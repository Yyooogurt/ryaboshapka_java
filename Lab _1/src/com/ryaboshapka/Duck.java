package com.ryaboshapka;

// Создаем класс уток и наследуем его от класса птиц
public class Duck extends Birds{
    //Создаем конструктор, в который передаем стандартные значения для всех птиц
    public Duck(String name, String food, String area) {
        //Вызываем конструктор супер класса
        super(name, food, area);
    }
    @Override
    public String swim() {
        return "Duck can floats over" + getArea();
    }
    @Override
    public String fly() {
        return "Duck can flies over" + getArea();
    }
    @Override
    public String eat() {
        return "Duck can eat this " + getFood();
    }
    @Override
    public String run() {
        return "Duck can runs over" + getArea();
    }
    @Override
    public String cry() {
        return "Duck can cry over" + getArea();

    }
}
