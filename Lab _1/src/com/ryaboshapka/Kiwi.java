package com.ryaboshapka;

// Создаем класс птиц Киви и наследуем его от класса птиц
public class Kiwi extends Birds{
    //Создаем конструктор, в который передаем стандартные значения для всех птиц
    public Kiwi(String name, String food, String area) {
        //Вызываем конструктор супер класса
        super(name, food, area);
    }
    @Override
    public String swim() {
        return "Kiwi can floats over" + getArea();
    }
    @Override
    public String fly() {
        return "Kiwi can flies over" + getArea();
    }
    @Override
    public String eat() {
        return "Kiwi can eat this " + getFood();
    }
    @Override
    public String run() {
        return "Kiwi can runs over" + getArea();
    }
    @Override
    public String cry() {
        return "Kiwi can cry over" + getArea();
    }
}
