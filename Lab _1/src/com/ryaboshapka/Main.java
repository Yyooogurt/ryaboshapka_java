package com.ryaboshapka;
public class Main {
    public static void main(String[] args) {
        // Создание объекта класса Activity
        Activity activity = new Activity();
        // Методы fly(), run(), swim(), eat (),cry()
        // должны вызываться из класса Main только через объект и методы класса Activity.
        activity.controller();
    }
}
