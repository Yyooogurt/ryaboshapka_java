package com.ryaboshapka;

// Создаем класс страуса и наследуем его от класса птиц
public class Ostrich extends Birds{
    //Создаем конструктор, в который передаем стандартные значения для всех птиц
    public Ostrich(String name, String food, String area) {
        //Вызываем конструктор супер класса
        super(name, food, area);
    }
    @Override
    public String swim() {
        return "Ostrich can floats over" + getArea();
    }
    @Override
    public String fly() {
        return "Ostrich can flies over" + getArea();
    }
    @Override
    public String eat() {
        return "Ostrich can eat this " + getFood();
    }
    @Override
    public String run() {
        return "Ostrich can runs over" + getArea();
    }
    @Override
    public String cry() {
        return "Ostrich can cry over" + getArea();
    }
}
