package com.ryaboshapka;

// Создаем класс пингвинов и наследуем его от класса птиц
public class Penguin extends Birds {
    //Создаем конструктор, в который передаем стандартные значения для всех птиц
    public Penguin(String name, String food, String area) {
        //Вызываем конструктор супер класса
        super(name, food, area);
    }
    @Override
    public String swim() {
        return "Penguin can floats over" + getArea();
    }
    @Override
    public String fly() {
        return "Penguin can flies over" + getArea();
    }
    @Override
    public String eat() {
        return "Penguin can eat this " + getFood();
    }
    @Override
    public String run() {
        return "Penguin can runs over" + getArea();
    }
    @Override
    public String cry() {
        return "Penguin can cry over" + getArea();
    }
}
