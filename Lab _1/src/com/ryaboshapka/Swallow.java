package com.ryaboshapka;

// Создаем класс ласточек и наследуем его от класса птиц
public class Swallow extends Birds{
    //Создаем конструктор, в который передаем стандартные значения для всех птиц
    public Swallow(String name, String food, String area) {
        //Вызываем конструктор супер класса
        super(name, food, area);
    }
    @Override
    public String swim() {
        return "Swallow can floats over" + getArea();
    }
    @Override
    public String fly() {
        return "Swallow can flies over" + getArea();
    }
    @Override
    public String eat() {
        return "Swallow can eat this " + getFood();
    }
    @Override
    public String run() {
        return "Swallow can runs over" + getArea();
    }
    @Override
    public String cry() {
        return "Swallow can cry over" + getArea();
    }
}
